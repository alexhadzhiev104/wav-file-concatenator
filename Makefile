all:
	gcc -c -o globals.o -g globals.c
	gcc -c -o wav.o -g wav.c
	gcc -c -o utils.o -g utils.c
	gcc -c -o main.o -g main.c
	gcc -o main -g main.o utils.o wav.o globals.o

clean:
	rm main.o 
	rm utils.o 
	rm globals.o 
	rm wav.o
	rm main
