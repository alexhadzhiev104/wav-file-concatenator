#ifndef GLOBALS_H
#define GLOBALS_H

#include <stdint.h>

#define DECIMAL 1
#define HEXADECIMAL 2
#define OCTAL 3

#define DECIMAL_STR "dec"
#define OCTAL_STR "oct"
#define HEXADECIMAL_STR "hex"

#define FIRST_FILE_ENTER_MSG "Enter path to the first file:\n-> "
#define SECOND_FILE_ENTER_MSG "Enter path to the second file:\n-> "

#define COMP_MSG_NOT_EQUAL "\nThe sound content of %s has %s more bytes sound data than %s.\n\n"
#define COMP_MSG_EQUAL "\nThe sound content of %s is the same size bytes of sound data as the one in %s.\n\n"
#define COMP_MSG_SAME_FILE "\nThe two files are the same.\n\n" 

#define POS_FILE_ENTER_MSG "Enter one of the two files:\n-> "
#define POSITION_ENTER_MSG "Enter a position:\n-> "

#define FILENAME_BUFFER_SIZE 512
#define POSITION_BUFFER_SIZE 64

#define FRONT_POS_STR "front"
#define BACK_POS_STR "back"
#define SAME_POS_STR "same"

#define OUT_FILENAME_ENDING "concatenated.wav"
#define OUT_FILENAME_SPRINTF_PATTERN "%s-%s-%s"

#define DATA_READ_PAGE 2048

extern uint8_t is_silent;

#endif
