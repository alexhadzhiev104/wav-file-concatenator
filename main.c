#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <fcntl.h>
#include <errno.h>

#include "utils.h"
#include "wav.h"

//------------------------------------------------------------------------
// FUNCTION: int main
// the program takes two wav file from the user input
// and combines them given the order provided by the user
// PARAMETERS:
// argc - number of arguments of the exec
// argv - array of exec arguments
//-----------------------------------------------------------------------

int
main ( int argc , char *argv[] )
{
	int8_t base = handle_args ( argc , argv , "sn" );

	if ( base < 0 )
	{	
		LOG_ERROR ( "arguments not handled properly." );

		return 1;
	}

	wavfile_t first_wavfile;
	wavfile_t second_wavfile;

	char *first_filename  = calloc ( FILENAME_BUFFER_SIZE , sizeof(char) );
	char *second_filename = calloc ( FILENAME_BUFFER_SIZE , sizeof(char) );

	get_input ( first_filename  , FILENAME_BUFFER_SIZE , FIRST_FILE_ENTER_MSG  );
	get_input ( second_filename , FILENAME_BUFFER_SIZE , SECOND_FILE_ENTER_MSG );

	int8_t first_result = fill_wav ( first_filename , &first_wavfile  );
	int8_t second_result = fill_wav ( second_filename , &second_wavfile );

	if ( first_result < 0 || second_result < 0 )
	{
		free ( first_filename  );
		free ( second_filename );

		if ( !( is_silent ) )
			LOG_ERROR ( "could not fill wav files properly." );

		return 1;
	}

	if ( !( is_silent ) )
		print_comparison ( first_wavfile , second_wavfile , base );

	char *input_filename = calloc ( FILENAME_BUFFER_SIZE , sizeof(char) );
	char *position = calloc ( POSITION_BUFFER_SIZE , sizeof(char) );

	if ( strcmp ( first_filename , second_filename ) )
	{
		uint8_t input_is_right = 0;
		
		while ( !( input_is_right ) )
		{
			get_input ( input_filename , FILENAME_BUFFER_SIZE , POS_FILE_ENTER_MSG );
			get_input ( position , POSITION_BUFFER_SIZE , POSITION_ENTER_MSG );

			input_is_right = is_concat_input_valid ( input_filename , position , first_wavfile.filename , second_wavfile.filename );
		}
		
		if ( !( is_silent ) )
			printf ( "\nYou chose %s at the %s.\n" , input_filename , position );
	}
	else
	{
		strncpy ( position , SAME_POS_STR , strlen ( SAME_POS_STR ) );

		if ( !( is_silent ) )
			printf ( "Concatenating the same file.\n" );
	}

	wavfilepos_t wavfilepos;

	if ( ( fill_wavfilepos ( &wavfilepos , position , input_filename ) ) < 0 )
	{	
		if ( !( is_silent ) )
			LOG_ERROR ( "could not fill wav file position." );
		
		return 1;
	}

	wavfile_t out_wavfile;

	if ( ( create_new_wav_file ( &out_wavfile , wavfilepos , first_wavfile , second_wavfile ) ) < 0 )
	{
		if ( !( is_silent ) )
			LOG_ERROR ( "could not create wav file properly." );

		return 1;
	}
	write_wav_file ( out_wavfile );

	free_wavfile ( &first_wavfile  );
	free_wavfile ( &second_wavfile );
	free_wavfile ( &out_wavfile    );

	free ( first_filename  );
	free ( second_filename );

	free ( input_filename );
	free ( position );

	free_wavfilepos ( &wavfilepos );

	return 0;
}
