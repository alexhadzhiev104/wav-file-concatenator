#include "utils.h"

int8_t
handle_args ( int argc , char *argv[] , const char *restrict optargs )
{
	char arg = 0;

	uint8_t base = 0;

	while ( arg != -1 )
	{
		
		switch ( arg )
		{
			case SILENT_OPT_CHAR:
				is_silent = 1;

				if ( base )
				{
					if ( ( argc - 1 ) <= 2 )
					{
						LOG_ERROR ( NOT_ENOUGH_ARGS_ERROR_MSG_STR );

						return -1;
					}
				}

				break;
			case NUM_BASE_OPT_CHAR:
				if ( is_silent )
				{
					if ( ( argc - 1 ) <= 2 )
					{
						LOG_ERROR ( NOT_ENOUGH_ARGS_ERROR_MSG_STR );
						
						return -1;
					}
				}
				else
				{
					if ( argc <= 2 )
					{
						LOG_ERROR ( NOT_ENOUGH_ARGS_ERROR_MSG_STR );

						return -1;
					}
				}

				for ( int i = 2 ; i < argc ; ++i )
				{
					if ( ( argv[i][0] != '-' ) && !( strcmp ( argv[i - 1] , NUM_BASE_OPT_STR ) ) )
					{
						if ( !( strcmp ( argv[i] , DECIMAL_STR ) ) )
							base = DECIMAL;
						else if ( !( strcmp ( argv[i] , HEXADECIMAL_STR ) ) )
							base = HEXADECIMAL;
						else if ( !( strcmp ( argv[i] , OCTAL_STR ) ) )
							base = OCTAL;
						else
						{
							LOG_ERROR ( WRONG_ARGUMENTS_ERROR_MSG_STR );
							
							return -1;
						}
					}
					else if ( ( argv[i][0] == '-' ) && !( strcmp ( argv[i - 1] , NUM_BASE_OPT_STR ) ) )
					{
						LOG_ERROR ( WRONG_ARGUMENTS_ERROR_MSG_STR );

						return -1;
					}
				}

				break;
		}
		arg = getopt ( argc , argv , optargs );
	}

	return base;
}

uint32_t
combine_lsb_parts ( uint8_t *bytes , uint8_t byte_numbers )
{
	if ( byte_numbers / 2 )
	{
		uint16_t *halves = calloc ( ( byte_numbers / 2 ) , sizeof(uint16_t) ); 

		if ( halves == NULL ) 
			if ( !( is_silent ) )
				LOG_ERROR ( strerror ( errno ) );

		for ( int i = 0 , j = 0 ; i < byte_numbers && j < byte_numbers / 2; i += 2 , j++ )
			halves[j] = ( bytes[i + 1] << 8 ) | ( bytes[i] & 0xFF );

		uint32_t value = ( ( halves[1] << 16 ) | ( halves[0] & 0xFFFF ) );

		free ( halves );

		return value;
	}
	else
	{
		return ( ( bytes[1] << 16 ) | ( bytes[0] & 0xFF ) );
	}

	return 0;
}

void
get_input ( char *restrict buffer , size_t buffer_size , const char *restrict msg )
{
	ssize_t bytes_written = write ( STDOUT_FILENO , msg , strlen ( msg ) );
	if ( bytes_written < 0 )
		if ( !( is_silent ) )
			LOG_ERROR ( strerror ( errno ) );

	ssize_t bytes_read = read ( STDIN_FILENO , buffer , buffer_size );
	if ( bytes_read < 0 )
		if ( !( is_silent ) )
			LOG_ERROR ( strerror ( errno ) );

	if ( bytes_read < buffer_size )
		buffer[bytes_read - 1] = '\0';
	else
		buffer[buffer_size - 1] = '\0';
}

uint8_t
is_concat_input_valid ( const char *restrict filename , const char *restrict position , 
	const char *restrict first_filename , const char *restrict second_filename )
{
	return ( ( !( strcmp ( position , FRONT_POS_STR ) ) || !( strcmp ( position , BACK_POS_STR ) ) ) 
		&& ( !( strcmp ( filename , first_filename ) ) || !( strcmp ( filename , second_filename ) ) ) );
}

char *
convert_to_base_str ( int number , uint8_t base )
{
	char *str = calloc ( 30 , sizeof(char) );
	if ( str == NULL )
	{
		if ( !( is_silent ) )
			LOG_ERROR ( strerror ( errno ) );

		return NULL;
	}
	
	switch ( base )
	{
		case DECIMAL: 
			sprintf ( str , "%d" , number );
			break;
		case HEXADECIMAL:
			sprintf ( str , "%02X" , number );
			break;
		case OCTAL:
			sprintf ( str , "%o" , number );
			break;
		default:
			sprintf ( str , "%d" , number );
	}

	return str;
}

char *
clean_filename ( const char *filename )
{
	return basename ( filename );
}

int8_t
fill_wavfilepos ( wavfilepos_t *wavfilepos , char *restrict position , char *restrict filename )
{
	wavfilepos->position = calloc ( strlen ( position ) + 1 , sizeof(char) );
	if ( wavfilepos->position == NULL )
	{	
		if ( !( is_silent ) )
			LOG_ERROR ( strerror ( errno ) );
		
		return -1;
	}

	wavfilepos->filename = calloc ( strlen ( filename ) + 1 , sizeof(char) );
	if ( wavfilepos->filename == NULL )
	{
		if ( !( is_silent ) )
			LOG_ERROR ( strerror ( errno ) );

		return -1;
	}

	strncpy ( wavfilepos->position , position , strlen ( position ) + 1);
	wavfilepos->position[strlen ( position )] = '\0';

	strncpy ( wavfilepos->filename , filename , strlen ( filename ) + 1);
	wavfilepos->filename[strlen ( filename )] = '\0';

	return 0;
}

void
free_wavfilepos ( wavfilepos_t *wavfilepos )
{
	free ( wavfilepos->position );
	free ( wavfilepos->filename );
}

void
create_out_filename ( char *restrict out_filename , wavfilepos_t wavfilepos ,
	const char *restrict first_filename , const char *restrict second_filename )
{
	if ( !( strcmp ( wavfilepos.position , SAME_POS_STR ) ) )
		sprintf ( out_filename , OUT_FILENAME_SPRINTF_PATTERN , first_filename , second_filename , OUT_FILENAME_ENDING );
	else
	{
		if ( !( strcmp ( wavfilepos.position , FRONT_POS_STR ) ) )
		{
			sprintf ( out_filename , OUT_FILENAME_SPRINTF_PATTERN , FIND_FILENAME ( wavfilepos.filename , first_filename , second_filename ) ,
				INVERSE_FIND_FILENAME ( wavfilepos.filename , first_filename , second_filename ) , OUT_FILENAME_ENDING );
		}
		else
		{
			sprintf ( out_filename , OUT_FILENAME_SPRINTF_PATTERN , INVERSE_FIND_FILENAME ( wavfilepos.filename , first_filename , second_filename ) ,
				FIND_FILENAME ( wavfilepos.filename , first_filename , second_filename ) , OUT_FILENAME_ENDING );
		}
	}

	out_filename[strlen ( out_filename )] = '\0';
}
