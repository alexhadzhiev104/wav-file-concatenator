#ifndef UTILS_H
#define UTILS_H

#define _GNU_SOURCE

#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <stdio.h>

#include "globals.h"

#define WRONG_ARGUMENTS_ERROR_MSG_STR "wrong arguments."
#define NOT_ENOUGH_ARGS_ERROR_MSG_STR "not enough arguments."

#define SILENT_OPT_CHAR 's'
#define NUM_BASE_OPT_CHAR 'n'

#define NUM_BASE_OPT_STR "-n"

#define FIND_FILENAME(filename, first_filename, second_filename) \
	( ( !( strcmp ( filename , first_filename ) ) ) ? ( first_filename ) : ( second_filename ) )

#define INVERSE_FIND_FILENAME(filename, first_filename, second_filename ) \
	( ( !( strcmp ( FIND_FILENAME(filename, first_filename, second_filename) , first_filename ) ) ) ? ( second_filename ) : ( first_filename ) )

#define FIND_WAVFILE(in_filename, first_wavfile, second_wavfile) \
	( ( !( strcmp ( in_filename , first_wavfile.filename ) ) ) ? ( first_wavfile ) : ( second_wavfile ) )

#define INVERSE_FIND_WAVFILE(in_filename , first_wavfile, second_wavfile) \
	( ( !( strcmp ( FIND_WAVFILE ( in_filename , first_wavfile , second_wavfile ).filename , first_wavfile.filename ) ) ? ( second_wavfile ) : ( first_wavfile ) ) )

#define LOG_ERROR( msg ) fprintf ( stderr , "[%s] ERROR: %s\n" , __func__ , msg )

typedef struct {
	char *position;
	char *filename;
} wavfilepos_t;

//------------------------------------------------------------------------
// FUNCTION: int8_t handle_args 
// this function processes exec agruments given to the main function
// PARAMETERS:
// argc - number of exec arguments
// argv - array of exec arguments
// optargs - a string with valid argument characters
//-----------------------------------------------------------------------
int8_t
handle_args ( int argc , char *argv[] , const char *restrict optargs );

//------------------------------------------------------------------------
// FUNCTION: uint32_t combine_lsb_parts
// this function takes an array of 1 byte numbers ( ordered lsb )
// and converts them to a single msb number
// PARAMETERS:
// bytes - array of 1 byte numbers
// byte_numbers - the number of elements in the bytes array
//-----------------------------------------------------------------------
uint32_t
combine_lsb_parts ( uint8_t *bytes , uint8_t byte_numbers );

//------------------------------------------------------------------------
// FUNCTION: void get_input
// this function takes a message and a buffer and writes the message to
// stdio and then processes input from the user and puts it into the
// buffer
// PARAMETERS:
// buffer - a string to store user input
// buffer_size , the maximum allowed input string
// msg - the message to be printed before taking input
//-----------------------------------------------------------------------
void
get_input ( char *restrict buffer , size_t buffer_size , const char *restrict msg );

//------------------------------------------------------------------------
// FUNCTION: uint8_t is_concat_input_valid
// this function takes a filename and a position and it checks if
// the position is valid and if the filename corresponds to one of the
// two wav files that are to be joined
// PARAMETERS:
// filename - filename taken from the user
// position - position of the filename, taken from user
// first_filename - the name of the first inputed file to be joined
// second_filename - the name of the second inputed file to be joined
//-----------------------------------------------------------------------
uint8_t
is_concat_input_valid ( const char *restrict filename , const char *restrict position , 
	const char *restrict first_filename , const char *restrict second_filename );

//------------------------------------------------------------------------
// FUNCTION: char *clean_filename
// takes a relative or full path to the file and cleans it so that
// only the name of the file remains
// PARAMETERS:
// filename - a relative or full path to the file
//-----------------------------------------------------------------------
char *
clean_filename ( const char *filename );

//------------------------------------------------------------------------
// FUNCTION: char *convert_to_base_str
// this function takes a number and a base ( allowed - OCT, HEX, DEC )
// and converts the given number to that base in string form using the 
// built in string printing options
// PARAMETERS:
// number - a number to be converted
// base - base the number to be converted to
//-----------------------------------------------------------------------
char *
convert_to_base_str ( int number , uint8_t base );

//------------------------------------------------------------------------
// FUNCTION: int8_t fill_wavfilepos
// this function takes a created object of type wavfilepos_t and it
// fills it with the given position and filename
// PARAMETERS:
// wavfilepos - the object the other parameters are to be written to
// position - the position of the file to be joined
// filename - the name of the file to be joined
//-----------------------------------------------------------------------
int8_t
fill_wavfilepos ( wavfilepos_t *wavfilepos , char *restrict position , char *restrict filename );

//------------------------------------------------------------------------
// FUNCTION: void free_wavfilepos
// this function frees the allocated memory in the wavfilepos object
// PARAMETERS:
// wavfilepos - the object whose elements are to be freed
//-----------------------------------------------------------------------
void
free_wavfilepos ( wavfilepos_t *wavfilepos );

//------------------------------------------------------------------------
// FUNCTION: void create_out_filename
// this function takes a position and the filenames of the two files
// that are to be joined and writes the corresponding filename to 
// out_filename
// PARAMETERS:
// out_filename - the filename of the joined file
// wavfilepos - the filename and position provided by the user
// first_filename - the name of the first file
// second_filename - the name of the second file
//-----------------------------------------------------------------------
void
create_out_filename ( char *restrict out_filename , wavfilepos_t wavfilepos ,
	const char *restrict first_filename , const char *restrict second_filename );

#endif
