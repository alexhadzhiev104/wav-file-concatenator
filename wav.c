#include "wav.h"

int8_t
fill_wav_info ( uint8_t *restrict buffer , wavfile_t *wavfile , char *restrict filename , int8_t data_offset )
{
	wavfile->filename = calloc ( strlen ( filename ) + 1 , sizeof(char) );

	strncpy ( wavfile->filename , filename , strlen ( filename ) );
	wavfile->filename[strlen ( filename )] = '\0';

	strncpy ( wavfile->wavchnk.chnk_id , buffer + WAV_CHNK_ID_OFFS , WAV_CHNK_ID_SIZE );
	wavfile->wavchnk.chnk_id[WAV_CHNK_ID_SIZE] = '\0';

	wavfile->wavchnk.chnk_size = combine_lsb_parts ( buffer + WAV_CHNK_SIZE_OFFS , WAV_CHNK_SIZE_SIZE );
	
	strncpy ( wavfile->wavchnk.chnk_format , buffer + WAV_CHNK_FORMAT_OFFS , WAV_CHNK_FORMAT_SIZE );
	wavfile->wavchnk.chnk_format[WAV_CHNK_FORMAT_SIZE] = '\0';

	strncpy ( wavfile->fmtsubchnk.subchnk_id , buffer + WAV_FMT_SUBCHNK_ID_OFFS , WAV_FMT_SUBCHNK_ID_SIZE  );
	wavfile->fmtsubchnk.subchnk_id[WAV_FMT_SUBCHNK_ID_SIZE] = '\0';

	wavfile->fmtsubchnk.subchnk_size    = combine_lsb_parts ( buffer + WAV_FMT_SUBCHNK_SIZE_OFFS         , WAV_FMT_SUBCHNK_SIZE_SIZE );
	wavfile->fmtsubchnk.audio_format    = combine_lsb_parts ( buffer + WAV_FMT_SUBCHNK_AUDIO_FORMAT_OFFS , WAV_FMT_SUBCHNK_AUDIO_FORMAT_SIZE );
	wavfile->fmtsubchnk.num_channels    = combine_lsb_parts ( buffer + WAV_FMT_SUBCHNK_NUMCHANNELS_OFFS  , WAV_FMT_SUBCHNK_NUMCHANNELS_SIZE );
	wavfile->fmtsubchnk.sample_rate     = combine_lsb_parts ( buffer + WAV_FMT_SUBCHNK_SAMPLE_RATE_OFFS  , WAV_FMT_SUBCHNK_SAMPLE_RATE_SIZE );
	wavfile->fmtsubchnk.byte_rate       = combine_lsb_parts ( buffer + WAV_FMT_SUBCHNK_BYTE_RATE_OFFS    , WAV_FMT_SUBCHNK_BYTE_RATE_SIZE );
	wavfile->fmtsubchnk.block_align     = combine_lsb_parts ( buffer + WAV_FMT_SUBCHNK_BLOCK_ALIGN_OFFS  , WAV_FMT_SUBCHNK_BLOCK_ALIGN_SIZE );
	wavfile->fmtsubchnk.bits_per_sample = combine_lsb_parts ( buffer + WAV_FMT_SUBCHNK_BPS_OFFS          , WAV_FMT_SUBCHNK_BPS_SIZE );

	if ( wavfile->fmtsubchnk.audio_format != WAV_FMT_SUBCHNK_PCM )
	{
		free ( wavfile->filename );

		if ( !( is_silent ) )
			LOG_ERROR ( "file not PCM format." );

		return -1;
	}

	strncpy ( wavfile->datasubchnk.subchnk_id , buffer + WAV_DATA_SUBCHNK_ID_OFFS + data_offset , WAV_DATA_SUBCHNK_ID_SIZE );
	wavfile->datasubchnk.subchnk_id[WAV_DATA_SUBCHNK_ID_SIZE] = '\0';
	
	wavfile->datasubchnk.subchnk_size = combine_lsb_parts ( buffer + WAV_DATA_SUBCHNK_SIZE_OFFS + data_offset , WAV_DATA_SUBCHNK_SIZE_SIZE );
	
	wavfile->datasubchnk.data = calloc ( wavfile->datasubchnk.subchnk_size , sizeof(uint8_t) );

	return 0;
}

void
print_wav_chnk_info ( wavchnk_t wavchnk )
{
	printf ( "chunk id: %s\n"     , wavchnk.chnk_id     );
	printf ( "chunk size: %d\n"   , wavchnk.chnk_size   );
	printf ( "chunk format: %s\n" , wavchnk.chnk_format );
}

void
print_wav_fmt_subchnk_info ( fmtsubchnk_t fmtsubchnk )
{
	printf ( "subchunk id: %s\n"        , fmtsubchnk.subchnk_id      );
	printf ( "subchunk size: %d\n"      , fmtsubchnk.subchnk_size    );
	printf ( "audio format: %d\n"       , fmtsubchnk.audio_format    );
	printf ( "number of channels: %d\n" , fmtsubchnk.num_channels    );
	printf ( "sample rate: %d\n"        , fmtsubchnk.sample_rate     );
	printf ( "byte rate: %d\n"          , fmtsubchnk.byte_rate       );
	printf ( "block align: %d\n"        , fmtsubchnk.block_align     );
	printf ( "bits per sample: %d\n"    , fmtsubchnk.bits_per_sample );
}

void
print_wav_data_subchnk_info ( datasubchnk_t datasubchnk )
{
	printf ( "subchunk id: %s\n"   , datasubchnk.subchnk_id   );
	printf ( "subchunk size: %d\n" , datasubchnk.subchnk_size );
}

void
print_wav_info ( wavfile_t wavfile )
{
	printf ( "\n-- %s --\n" , wavfile.filename );

	print_wav_chnk_info ( wavfile.wavchnk );

	print_wav_fmt_subchnk_info ( wavfile.fmtsubchnk );

	print_wav_data_subchnk_info ( wavfile.datasubchnk );
}

void
fill_wav_data ( int fd , wavfile_t *wavfile )
{
	uint8_t *buffer = calloc ( wavfile->datasubchnk.subchnk_size , sizeof(uint8_t) );
	if ( buffer == NULL )
		if ( !( is_silent ) )
			LOG_ERROR ( strerror ( errno ) );

	ssize_t bytes_read = 1;
	ssize_t bytes_read_total = 0;

	while ( bytes_read != 0 )
	{
		bytes_read = read ( fd , buffer , DATA_READ_PAGE );
		if ( bytes_read < 0 )
			if ( !( is_silent ) )
				LOG_ERROR ( strerror ( errno ) );

		memcpy ( wavfile->datasubchnk.data + bytes_read_total , buffer , bytes_read );

		bytes_read_total += bytes_read;
	}

	free ( buffer );
}

int8_t
fill_wav ( const char *filename , wavfile_t *wavfile )
{
	int fd = open ( filename , O_RDONLY );
	if ( fd < 0 )
	{
		if ( !( is_silent ) )
			LOG_ERROR ( strerror ( errno ) );

		return -1;
	}

	uint8_t *buffer = calloc ( WAV_CHNK_SIZE + WAV_FMT_SUBCHNK_SIZE + 
		WAV_DATA_SUBCHNK_ID_SIZE + WAV_DATA_SUBCHNK_SIZE_SIZE , sizeof(uint8_t) );
	if ( buffer == NULL )
	{
		if ( !( is_silent ) )
			LOG_ERROR ( strerror ( errno ) );

		if ( close ( fd ) < 0 )
			if ( !( is_silent ) )
				LOG_ERROR ( strerror ( errno ) );

		return -1;
	}

	ssize_t bytes_read;

	if ( ( bytes_read = read ( fd , buffer , WAV_CHNK_SIZE + WAV_FMT_SUBCHNK_SIZE + 
			WAV_DATA_SUBCHNK_ID_SIZE + WAV_DATA_SUBCHNK_SIZE_SIZE ) ) < 0 )
	{
		free ( buffer );

		if ( !( is_silent ) )
			LOG_ERROR ( strerror ( errno ) );

		if ( close ( fd ) < 0 )
			if ( !( is_silent ) )
				LOG_ERROR ( strerror ( errno ) );

		return -1;
	}

	char *tmp_string = calloc ( 5 , sizeof(char) );
	if ( tmp_string == NULL )
	{
		free ( buffer );

		if ( !( is_silent ) )
			LOG_ERROR ( strerror ( errno ) );

		if ( close ( fd ) < 0 )
			if ( !( is_silent ) )
				LOG_ERROR ( strerror ( errno ) );

		return -1;
	}

	strncpy ( tmp_string , buffer + WAV_CHNK_ID_OFFS , WAV_CHNK_ID_SIZE );
	tmp_string[WAV_CHNK_FORMAT_SIZE] = '\0';

	if ( strcmp ( tmp_string , "RIFF" ) )
	{
		free ( buffer );
		free ( tmp_string );

		if ( !( is_silent ) )
			LOG_ERROR ( "not RIFF format." );

		if ( close ( fd ) < 0 )
			if ( !( is_silent ) )
				LOG_ERROR ( strerror ( errno ) );

		return -1;
	}

	strncpy ( tmp_string , buffer + WAV_CHNK_FORMAT_OFFS , WAV_CHNK_FORMAT_SIZE );
	tmp_string[WAV_CHNK_FORMAT_SIZE] = '\0';

	if ( strcmp ( tmp_string , "WAVE" ) )
	{
		free ( buffer );
		free ( tmp_string );

		if ( !( is_silent ) )
			LOG_ERROR ( "not WAVE format." );
	
		if ( close ( fd ) < 0 )
			if ( !( is_silent ) )
				LOG_ERROR ( strerror ( errno ) );

		return -1;
	}

	int fmtsubchnk_size = combine_lsb_parts ( buffer + WAV_FMT_SUBCHNK_SIZE_OFFS , WAV_FMT_SUBCHNK_SIZE_SIZE );

	int data_offset = 0;

	if ( fmtsubchnk_size != WAV_FMT_SUBCHNK_STANDARD_SIZE )
	{
		int diff = fmtsubchnk_size - WAV_FMT_SUBCHNK_STANDARD_SIZE;

		buffer = realloc ( buffer , WAV_CHNK_SIZE + WAV_FMT_SUBCHNK_SIZE + WAV_DATA_SUBCHNK_ID_SIZE + WAV_DATA_SUBCHNK_SIZE_SIZE + diff );

		lseek ( fd , 0 , SEEK_SET );
		if ( ( bytes_read = read ( fd , buffer , WAV_CHNK_SIZE + WAV_FMT_SUBCHNK_SIZE + 
				WAV_DATA_SUBCHNK_ID_SIZE + WAV_DATA_SUBCHNK_SIZE_SIZE + diff ) ) < 0 )
		{
			free ( buffer );
			free ( tmp_string );

			if ( !( is_silent ) )
				LOG_ERROR ( strerror ( errno ) );

			if ( close ( fd ) < 0 )
				if ( !( is_silent ) )
					LOG_ERROR ( strerror ( errno ) );

			return -1;
		}

		data_offset = diff;
	}

	if ( ( fill_wav_info ( buffer , wavfile , clean_filename ( filename ) , data_offset ) ) < 0 )
	{
		free ( buffer );
		free ( tmp_string );

		if ( !( is_silent ) )
			LOG_ERROR ( "could not fill WAV info." );

		if ( close ( fd ) < 0 )
			if ( !( is_silent ) )
				LOG_ERROR ( strerror ( errno ) );

		return -1;
	}
	
	fill_wav_data ( fd , wavfile );
	
	if ( !( is_silent ) )
		print_wav_info ( *wavfile );

	free ( buffer );
	free ( tmp_string );

	if ( close ( fd ) < 0 )
	{
		if ( !( is_silent ) )
			LOG_ERROR ( strerror ( errno ) );
		
		return -1;
	}

	return 0;
}

void 
print_comparison ( wavfile_t first_wavfile , wavfile_t second_wavfile , int base )
{
	int comp = ( first_wavfile.datasubchnk.subchnk_size - second_wavfile.datasubchnk.subchnk_size );

	char *base_str = convert_to_base_str ( abs ( comp ) , base );

	( comp > 0 ) ? ( printf ( COMP_MSG_NOT_EQUAL , first_wavfile.filename , base_str , second_wavfile.filename ) ) 
		: ( ( comp < 0 ) ? ( printf ( COMP_MSG_NOT_EQUAL , second_wavfile.filename , base_str , first_wavfile.filename ) ) 
				: ( ( strcmp ( first_wavfile.filename , second_wavfile.filename ) ) ? ( printf ( COMP_MSG_EQUAL , first_wavfile.filename , second_wavfile.filename ) ) 
					: ( printf ( COMP_MSG_SAME_FILE ) ) ) );

	free ( base_str );
}

void
fill_new_wav_chnk_info ( wavchnk_t *wavchnk )
{
	strncpy ( wavchnk->chnk_id , WAV_CHNK_ID , WAV_CHNK_ID_SIZE );
	wavchnk->chnk_id[WAV_CHNK_ID_SIZE] = '\0';

	strncpy ( wavchnk->chnk_format , WAV_CHNK_FORMAT , WAV_CHNK_FORMAT_SIZE );
	wavchnk->chnk_format[WAV_CHNK_FORMAT_SIZE] = '\0';
}

void
fill_new_wav_fmt_subchnk_info ( fmtsubchnk_t *fmtsubchnk , fmtsubchnk_t fmtsubchnk_info )
{
	strncpy ( fmtsubchnk->subchnk_id , WAV_FMT_SUBCHNK_ID , WAV_FMT_SUBCHNK_ID_SIZE );
	fmtsubchnk->subchnk_id[WAV_FMT_SUBCHNK_ID_SIZE] = '\0';

	fmtsubchnk->audio_format    = fmtsubchnk_info.audio_format;
	fmtsubchnk->num_channels    = fmtsubchnk_info.num_channels;
	fmtsubchnk->sample_rate     = fmtsubchnk_info.sample_rate;
	fmtsubchnk->byte_rate       = fmtsubchnk_info.byte_rate;
	fmtsubchnk->block_align     = fmtsubchnk_info.block_align;
	fmtsubchnk->bits_per_sample = fmtsubchnk_info.bits_per_sample;
}

void
fill_new_wav_data_subchnk_info ( datasubchnk_t *datasubchnk )
{
	strncpy ( datasubchnk->subchnk_id , WAV_DATA_SUBCHNK_ID , WAV_DATA_SUBCHNK_ID_SIZE );
	datasubchnk->subchnk_id[WAV_DATA_SUBCHNK_ID_SIZE] = '\0';
}

void
fill_new_wav_info ( wavfile_t *wavfile , const char *filename , wavfile_t wavfile_info )
{
	wavfile->filename = calloc ( strlen ( filename ) + 1 , sizeof(char) );

	strncpy ( wavfile->filename , filename , strlen ( filename ) );
	wavfile->filename[strlen ( filename )] = '\0';

	fill_new_wav_chnk_info ( &( wavfile->wavchnk ) );

	fill_new_wav_fmt_subchnk_info ( &( wavfile->fmtsubchnk ) , wavfile_info.fmtsubchnk );

	fill_new_wav_data_subchnk_info ( &( wavfile->datasubchnk ) );
}

void
fill_new_wav_sizes ( wavfile_t *wavfile , uint32_t data_size )
{
	wavfile->datasubchnk.subchnk_size = data_size;

	wavfile->fmtsubchnk.subchnk_size = WAV_FMT_SUBCHNK_SIZE - WAV_FMT_SUBCHNK_ID_SIZE - WAV_FMT_SUBCHNK_SIZE_SIZE;

	wavfile->wavchnk.chnk_size = data_size + WAV_CHNK_SIZE + WAV_FMT_SUBCHNK_SIZE + 
			WAV_DATA_SUBCHNK_ID_SIZE + WAV_DATA_SUBCHNK_SIZE_SIZE;
}

void
fill_new_wav_data ( wavfile_t *out_wavfile , wavfile_t first_wavfile , 
	wavfile_t second_wavfile , wavfile_t third_wavfile )
{
	uint32_t size = first_wavfile.datasubchnk.subchnk_size +
		second_wavfile.datasubchnk.subchnk_size + third_wavfile.datasubchnk.subchnk_size;

	out_wavfile->datasubchnk.data = calloc ( size , sizeof(uint8_t) );
	if ( out_wavfile->datasubchnk.data == NULL )
		if ( !( is_silent ) )
			LOG_ERROR ( strerror ( errno ) );

	memcpy ( out_wavfile->datasubchnk.data , first_wavfile.datasubchnk.data , 
			first_wavfile.datasubchnk.subchnk_size );

	memcpy ( out_wavfile->datasubchnk.data + first_wavfile.datasubchnk.subchnk_size , 
			second_wavfile.datasubchnk.data , second_wavfile.datasubchnk.subchnk_size );

	memcpy ( out_wavfile->datasubchnk.data + first_wavfile.datasubchnk.subchnk_size + 
			second_wavfile.datasubchnk.subchnk_size , third_wavfile.datasubchnk.data , third_wavfile.datasubchnk.subchnk_size );

	fill_new_wav_sizes ( out_wavfile , size );
}

void
generate_silence ( wavfile_t *wavfile , wavfile_t wav_info , uint32_t seconds )
{
	wavfile->filename = calloc ( FILENAME_BUFFER_SIZE , sizeof(char) );
	strncpy ( wavfile->filename , SILENCE_FILENAME_STR , strlen ( SILENCE_FILENAME_STR ) );

	uint32_t size = seconds * wav_info.fmtsubchnk.byte_rate;

	wavfile->datasubchnk.data = calloc ( size , sizeof(uint8_t) );

	wavfile->datasubchnk.subchnk_size = size;

	memset ( wavfile->datasubchnk.data , size , 0 );
}

wavfile_t *
comp_wav_files ( wavfile_t first_wavfile , wavfile_t second_wavfile )
{
	wavfile_t *wav_info = malloc ( sizeof(wavfile_t) );

	if ( first_wavfile.fmtsubchnk.audio_format != second_wavfile.fmtsubchnk.audio_format )
	{
		if ( !( is_silent ) )
			LOG_ERROR ( "audio_format is different." );

		free ( wav_info );

		return NULL;
	}
	else if ( first_wavfile.fmtsubchnk.num_channels != second_wavfile.fmtsubchnk.num_channels )
	{
		if ( !( is_silent ) )
			LOG_ERROR ( "number of channels is different." );

		free ( wav_info );

		return NULL;
	}
	else if ( first_wavfile.fmtsubchnk.sample_rate != second_wavfile.fmtsubchnk.sample_rate )
	{
		if ( !( is_silent ) )
			LOG_ERROR ( "sample rate is different." );

		free ( wav_info );

		return NULL;
	}
	else if ( first_wavfile.fmtsubchnk.byte_rate != second_wavfile.fmtsubchnk.byte_rate )
	{
		if ( !( is_silent ) )
			LOG_ERROR ( "byte rate is different." );

		free ( wav_info );

		return NULL;
	}
	else if ( first_wavfile.fmtsubchnk.block_align != second_wavfile.fmtsubchnk.block_align )
	{
		if ( !( is_silent ) )
			LOG_ERROR ( "block align is different." );

		free ( wav_info );

		return NULL;
	}
	else if ( first_wavfile.fmtsubchnk.bits_per_sample != second_wavfile.fmtsubchnk.bits_per_sample )
	{
		if ( !( is_silent ) )
			LOG_ERROR ( "bit per sample is different." );

		free ( wav_info );

		return NULL;
	}

	wav_info->fmtsubchnk.audio_format    = first_wavfile.fmtsubchnk.audio_format;
	wav_info->fmtsubchnk.num_channels    = first_wavfile.fmtsubchnk.num_channels;
	wav_info->fmtsubchnk.sample_rate     = first_wavfile.fmtsubchnk.sample_rate;
	wav_info->fmtsubchnk.byte_rate       = first_wavfile.fmtsubchnk.byte_rate;
	wav_info->fmtsubchnk.block_align     = first_wavfile.fmtsubchnk.block_align;
	wav_info->fmtsubchnk.bits_per_sample = first_wavfile.fmtsubchnk.bits_per_sample;

	return wav_info;
}

int8_t
create_new_wav_file ( wavfile_t *wavfile , wavfilepos_t wavfilepos , 
	wavfile_t first_wavfile , wavfile_t second_wavfile )
{
	char *filename = calloc ( FILENAME_BUFFER_SIZE , sizeof(char) );
	if ( filename == NULL )
	{	
		if ( !( is_silent ) )
			LOG_ERROR ( strerror ( errno ) );

		return -1;
	}

	wavfile_t *wav_info = comp_wav_files ( first_wavfile , second_wavfile );
	if ( wav_info == NULL )
	{
		if ( !( is_silent ) )
			LOG_ERROR ( "the two wav files differ." );

		free ( filename );

		return -1;
	}

	create_out_filename ( filename , wavfilepos , 
		first_wavfile.filename , second_wavfile.filename );

	fill_new_wav_info ( wavfile , filename , *( wav_info ) );

	wavfile_t wav_silence;

	generate_silence ( &wav_silence , *wav_info , 5 );

	if ( !( strcmp ( wavfilepos.position , SAME_POS_STR ) ) )
	{
		fill_new_wav_data ( wavfile , first_wavfile , wav_silence , second_wavfile );
	}
	else
	{
		if ( ! ( strcmp ( wavfilepos.position , FRONT_POS_STR ) ) ) 
		{
			fill_new_wav_data ( wavfile , ( FIND_WAVFILE ( wavfilepos.filename , first_wavfile , second_wavfile ) ) , 
				wav_silence , ( INVERSE_FIND_WAVFILE ( wavfilepos.filename , first_wavfile , second_wavfile ) ) );
		}
		else
		{
			fill_new_wav_data ( wavfile , ( INVERSE_FIND_WAVFILE ( wavfilepos.filename , first_wavfile , second_wavfile ) ) ,
				wav_silence , ( FIND_WAVFILE ( wavfilepos.filename , first_wavfile , second_wavfile ) ) );
		}
	}

	if ( !( is_silent ) )
		print_wav_info ( *wavfile );

	free ( filename );
	free ( wav_info );

	free_wavfile ( &wav_silence );

	return 0;
}

int8_t
write_wav_file ( wavfile_t wavfile )
{
	int fd = open ( wavfile.filename , O_RDWR | O_CREAT , 0777 );
	if ( fd < 0 )
	{
		if ( !( is_silent ) )
			LOG_ERROR ( strerror ( errno ) );

		return -1;
	}

	ssize_t bytes_written = 0;

	bytes_written = write ( fd , wavfile.wavchnk.chnk_id , WAV_CHNK_ID_SIZE );
	bytes_written = write ( fd , &wavfile.wavchnk.chnk_size , WAV_CHNK_SIZE_SIZE );
	bytes_written = write ( fd , wavfile.wavchnk.chnk_format , WAV_CHNK_FORMAT_SIZE );
	
	bytes_written = write ( fd , wavfile.fmtsubchnk.subchnk_id , WAV_FMT_SUBCHNK_ID_SIZE );
	bytes_written = write ( fd , &wavfile.fmtsubchnk.subchnk_size , WAV_FMT_SUBCHNK_SIZE_SIZE );
	bytes_written = write ( fd , &wavfile.fmtsubchnk.audio_format , WAV_FMT_SUBCHNK_AUDIO_FORMAT_SIZE );
	bytes_written = write ( fd , &wavfile.fmtsubchnk.num_channels , WAV_FMT_SUBCHNK_NUMCHANNELS_SIZE );
	bytes_written = write ( fd , &wavfile.fmtsubchnk.sample_rate , WAV_FMT_SUBCHNK_SAMPLE_RATE_SIZE );
	bytes_written = write ( fd , &wavfile.fmtsubchnk.byte_rate , WAV_FMT_SUBCHNK_BYTE_RATE_SIZE );
	bytes_written = write ( fd , &wavfile.fmtsubchnk.block_align , WAV_FMT_SUBCHNK_BLOCK_ALIGN_SIZE );
	bytes_written = write ( fd , &wavfile.fmtsubchnk.bits_per_sample , WAV_FMT_SUBCHNK_BPS_SIZE );

	bytes_written = write ( fd , wavfile.datasubchnk.subchnk_id , WAV_DATA_SUBCHNK_ID_SIZE );
	bytes_written = write ( fd , &wavfile.datasubchnk.subchnk_size , WAV_DATA_SUBCHNK_SIZE_SIZE );
	bytes_written = write ( fd , wavfile.datasubchnk.data , wavfile.datasubchnk.subchnk_size );

	if ( bytes_written < 0 )
	{
		if ( !( is_silent ) )
			LOG_ERROR ( strerror ( errno ) );

		if ( close ( fd ) < 0 )
			if ( !( is_silent ) )
				LOG_ERROR ( strerror ( errno ) );

		return -1;
	}

	if ( close ( fd ) < 0 )
	{
		if ( !( is_silent ) )
			LOG_ERROR ( strerror ( errno ) );
		
		return -1;
	}

	return 0;
}

void
free_wavfile ( wavfile_t *wavfile )
{
	free ( wavfile->filename );
	free ( wavfile->datasubchnk.data );
}
