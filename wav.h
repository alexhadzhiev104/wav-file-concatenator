#ifndef WAV_H
#define WAV_H

#include <stdint.h>
#include <stdio.h>
#include <fcntl.h>
#include <sys/types.h>
#include <byteswap.h>

#include "utils.h"

#define WAV_CHNK_SIZE 12
#define WAV_CHNK_ID_SIZE 4
#define WAV_CHNK_SIZE_SIZE 4
#define WAV_CHNK_FORMAT_SIZE 4

#define WAV_FMT_SUBCHNK_SIZE 24
#define WAV_FMT_SUBCHNK_PCM 1
#define WAV_FMT_SUBCHNK_STANDARD_SIZE 16
#define WAV_FMT_SUBCHNK_ID_SIZE 4
#define WAV_FMT_SUBCHNK_SIZE_SIZE 4
#define WAV_FMT_SUBCHNK_AUDIO_FORMAT_SIZE 2
#define WAV_FMT_SUBCHNK_NUMCHANNELS_SIZE 2
#define WAV_FMT_SUBCHNK_SAMPLE_RATE_SIZE 4
#define WAV_FMT_SUBCHNK_BYTE_RATE_SIZE 4
#define WAV_FMT_SUBCHNK_BLOCK_ALIGN_SIZE 2
#define WAV_FMT_SUBCHNK_BPS_SIZE 2

#define WAV_DATA_SUBCHNK_ID_SIZE 4
#define WAV_DATA_SUBCHNK_SIZE_SIZE 4

#define WAV_CHNK_START_OFFS 0
#define WAV_CHNK_ID_OFFS 0
#define WAV_CHNK_SIZE_OFFS 4
#define WAV_CHNK_FORMAT_OFFS 8

#define WAV_FMT_SUBCHNK_START_OFFS 12
#define WAV_FMT_SUBCHNK_ID_OFFS 12 
#define WAV_FMT_SUBCHNK_SIZE_OFFS 16
#define WAV_FMT_SUBCHNK_AUDIO_FORMAT_OFFS 20
#define WAV_FMT_SUBCHNK_NUMCHANNELS_OFFS 22
#define WAV_FMT_SUBCHNK_SAMPLE_RATE_OFFS 24
#define WAV_FMT_SUBCHNK_BYTE_RATE_OFFS 28
#define WAV_FMT_SUBCHNK_BLOCK_ALIGN_OFFS 32
#define WAV_FMT_SUBCHNK_BPS_OFFS 34

#define WAV_DATA_SUBCHNK_ID_OFFS 36
#define WAV_DATA_SUBCHNK_SIZE_OFFS 40
#define WAV_DATA_SUBCHNK_DATA_OFFS 44

#define WAV_CHNK_ID "RIFF"
#define WAV_CHNK_FORMAT "WAVE"
#define WAV_FMT_SUBCHNK_ID "fmt "
#define WAV_DATA_SUBCHNK_ID "data"

#define SILENCE_FILENAME_STR "silence"

typedef struct {
	char     chnk_id[5];
	uint32_t chnk_size;
	char     chnk_format[5];
} wavchnk_t;

typedef struct {
	char     subchnk_id[5];
	uint32_t subchnk_size;
	uint16_t audio_format;
	uint16_t num_channels;
	uint32_t sample_rate;
	uint32_t byte_rate;
	uint16_t block_align;
	uint16_t bits_per_sample;
} fmtsubchnk_t;

typedef struct {
	char      subchnk_id[5];
	uint32_t  subchnk_size;
	uint8_t  *data;
} datasubchnk_t;

typedef struct {
	char         *filename;
	wavchnk_t     wavchnk;
	fmtsubchnk_t  fmtsubchnk;
	datasubchnk_t datasubchnk;
} wavfile_t;

//------------------------------------------------------------------------
// FUNCTION: void fill_wav_info
// this function takes already read data from the relevant wav file and
// it writes all the information from it into a wavfile_t object
// PARAMETERS:
// buffer - the buffer where the already read wav information is stored
// wavfile - the object where that information is going to be written
// filename - the name of the file being read
// data_offset - the offset of the data given the difference in fmt 
// subchunk sizes
//-----------------------------------------------------------------------
int8_t
fill_wav_info ( uint8_t *restrict buffer , wavfile_t *wavfile , char *restrict filename , int8_t data_offset );

//------------------------------------------------------------------------
// FUNCTION: void print_wav_chnk_info
// this function takes a wav chunk and prints all its relevant
// information
// PARAMETERS:
// wavchnk - the wav chunk that contains the information
//-----------------------------------------------------------------------
void
print_wav_chnk_info ( wavchnk_t wavchnk );

//------------------------------------------------------------------------
// FUNCTION: void print_wav_fmt_subchnk_info
// this function takes an fmt subchunk and prints all its relevant
// information
// PARAMETERS:
// fmtsubchnk - the fmt subchunk that contains the information
//-----------------------------------------------------------------------
void
print_wav_fmt_subchnk_info ( fmtsubchnk_t fmtsubchnk );

//------------------------------------------------------------------------
// FUNCTION: void get_input
// this function takes a data subchunk and prints all its relevant
// information
// PARAMETERS:
// datasubchnk - the data subchunk that contains the information
//-----------------------------------------------------------------------
void
print_wav_data_subchnk_info ( datasubchnk_t datasubchnk );

//------------------------------------------------------------------------
// FUNCTION: void print_wav_info
// this function takes a wav file and prints all relevant 
// information about it
// PARAMETERS:
// wavfile - the object whose information is to be printed
//-----------------------------------------------------------------------
void
print_wav_info ( wavfile_t wavfile );

//------------------------------------------------------------------------
// FUNCTION: void fill_wav_data
// this function reads sound data from a wav file and writes it into
// an object
// PARAMETERS:
// fd - the file descriptor of the open wav file
// wavfile - the object which stores the data of the open file
//-----------------------------------------------------------------------
void
fill_wav_data ( int fd , wavfile_t *wavfile );

//------------------------------------------------------------------------
// FUNCTION: int8_t fill_wav
// this function opens a wav file and reads the information before
// filling it and then reads the rest of the data
// PARAMETERS:
// filename - the name of the file which contains the wav data
// wavfile - the object where the read data is stored
//-----------------------------------------------------------------------
int8_t
fill_wav ( const char *filename , wavfile_t *wavfile );

//------------------------------------------------------------------------
// FUNCTION: void get_input
// this function compares the data sizes of the two files in a given base
// PARAMETERS:
// first_wavfile - the first file to be compared
// second_wavfile - the second file to be compared
// base - the base in which the comparison is printed
//-----------------------------------------------------------------------
void 
print_comparison ( wavfile_t first_wavfile , wavfile_t second_wavfile , int base );

//------------------------------------------------------------------------
// FUNCTION: void fill_new_wav_chnk_info
// this function writes the general wav chunk information 
// PARAMETERS:
// wavchnk - the main chunk part where the information is written
//-----------------------------------------------------------------------
void
fill_new_wav_chnk_info ( wavchnk_t *wavchnk );

//------------------------------------------------------------------------
// FUNCTION: void fill_new_wav_fmt_subchnk_info
// this function takes fmt information about the wav file and it writes it 
// into a given fmtsubchnk
// PARAMETERS:
// fmtsubchnk - where the information is written
// fmtsubchnk_info - the information about the file
//-----------------------------------------------------------------------
void
fill_new_wav_fmt_subchnk_info ( fmtsubchnk_t *fmtsubchnk , fmtsubchnk_t fmtsubchnk_info );

//------------------------------------------------------------------------
// FUNCTION: void fill_new_wav_data_subchnk_info
// this function writes data subchnk information into the given object
// PARAMETERS:
// datasubchnk - the data subchnk where the information is written
//-----------------------------------------------------------------------
void
fill_new_wav_data_subchnk_info ( datasubchnk_t *datasubchnk );

//------------------------------------------------------------------------
// FUNCTION: void fill_new_wav_info
// this function fills the wav info for the joint file into the finall 
// object
// PARAMETERS:
// wavfile - the joint object where the info is written
// filename - the name of the file
// wavfile_info - the general wav info data ( fmtsubchnk )
//-----------------------------------------------------------------------
void
fill_new_wav_info ( wavfile_t *wavfile , const char *filename , wavfile_t wavfile_info );

//------------------------------------------------------------------------
// FUNCTION: void fill_new_wav_sizes
// this function takes a wavfile_t object with already filled data
// and it fill its sizes accordingly
// PARAMETERS:
// wavfile - the object that stores tha joint data 
// size - the size of the joint data
//-----------------------------------------------------------------------
void
fill_new_wav_sizes ( wavfile_t *wavfile , uint32_t data_size );

//------------------------------------------------------------------------
// FUNCTION: void fill_new_wav_data
// this function takes 3 wavfile_t objects - the 2 files to be joined 
// and the silence between them and writes them into one object
// PARAMETERS:
// wavfile - the object where the combined data is stored
// first_wavfile - the first file 
// second_wavfile - the silence
// third_wavfile - the second file
//-----------------------------------------------------------------------
void
fill_new_wav_data ( wavfile_t *wavfile , wavfile_t first_wavfile , 
		wavfile_t second_wavfile , wavfile_t third_wavfile );

//------------------------------------------------------------------------
// FUNCTION: void generate_silence
// this function writes a given amount of seconds as silence to be used
// between two files when joining them
// PARAMETERS:
// wavfile - this is the object where the silence data will be saved
// wav_info - information about the wav file
// seconds - the duration of the silence
//-----------------------------------------------------------------------
void
generate_silence ( wavfile_t *wavfile , wavfile_t wav_info , uint32_t seconds );

//------------------------------------------------------------------------
// FUNCTION: int8_t create_new_wav_file
// this function takes an the two files to be joined and their relavite
// position and writes their joint data into wavfile
// PARAMETERS:
// wavfile - the data of the two joint files
// wavfilepos - the relative position of the files
// first_wavfile - the first file to be joined
// second_wavfile - the second file to be joined
//-----------------------------------------------------------------------
int8_t
create_new_wav_file ( wavfile_t *wavfile , wavfilepos_t wavfilepos , 
	wavfile_t first_wavfile , wavfile_t second_wavfile );

//------------------------------------------------------------------------
// FUNCTION: int8_t write_wav_file
// this function takes a wav file and it writes its data to the system
// PARAMETERS:
// wavfile - the wav file to be written on the system
//-----------------------------------------------------------------------
int8_t
write_wav_file ( wavfile_t wavfile );

//------------------------------------------------------------------------
// FUNCTION: void free_wavfile
// takes a pointer to a wavfile_t object and it frees its dynamic memory
// PARAMETERS:
// wavfile - the object whose elements are to be freed
//-----------------------------------------------------------------------
void
free_wavfile ( wavfile_t *wavfile );

#endif
